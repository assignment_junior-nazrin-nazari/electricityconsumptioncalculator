<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Electricity Consumption Calculator</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <script>
        function resetResults() {
            document.getElementById("results").innerHTML = "";
        }
    </script>
</head>

<body class="bg-light">
    <div class="container mt-5">
    <div class="row justify-content-center">
    <div class="col-md-6">
        <h1 class="text-center">Electricity Consumption Calculator<br></h1>
            
        <form action="index.php" method="post">

            <div class="form-group">
                <label>Voltage(V)<br></label>
                <input type="text" name="voltage" class="form-control" required>           
            </div>
    
            <div class="form-group">
                <label>Current(Ampere(A))<br></label>
                <input type="text" name="current" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Current Rate(sen/kWh)<br><a href="https://www.tnb.com.my/residential/pricing-tariffs#/">To Know About Current Rate(TNB Tariff Rate)</a><br></label>
                <input type="text" name="current_rate" class="form-control">
            </div>

            <div class="form-group">
                <label>Hours of using<br></label>
                <input type="text" name="Hours" class="form-control">
            </div>

            <div class="form-group">
                <label>Days of using<br></label>
                <input type="text" name="Days" class="form-control">
            </div>
                
            <div class="form-group text-center">
                <input type="submit" value="Calculate" class="btn btn-primary">
                <input type="reset" value="Reset" onclick="resetResults()" class="btn btn-secondary">
            </div>

            </form>

    <div id="results" class="mt-3">

        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $voltage = floatval($_POST["voltage"]);
            $current = floatval($_POST["current"]);
            
                if($voltage !== null && $current !== null) {
                    $current_rate = floatval($_POST["current_rate"]);
                    $hours = floatval($_POST["Hours"]);
                    $days = intval($_POST["Days"]);

                    $power = $voltage * $current;
                    $energy = $power * $hours / 1000;
                    $total = round($energy * ($current_rate/100) *$days, 2);
        ?>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">Descriptions</th>
                            <th scope="col">Results</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Your Power</td>
                            <td><?php echo"{$power}W<br>";?></php></td>
                        </tr>
                        <tr>
                            <td>Your Current Rate</td>
                            <td><?php echo"RM{$current_rate}<br>";?></php></td>
                        </tr>
                        <tr>
                            <td>Your Energy</td>
                            <td><?php echo"{$energy} kWh<br>";?></php></td>
                        </tr>
                        <tr>
                            <td>Your Total for <?php echo" {$days} days";?></td>
                            <td><?php echo" RM{$total}<br>";?></php></td>
                        </tr>
                                        
            <?php
                    } else {
                        echo"Please fill the form";
                    }
                }
            ?>
    </div>
    </div>
    </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>
